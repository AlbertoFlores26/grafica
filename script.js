(async () => {
    const respuestaRaw = await fetch("./consultaDatos.php");
    const respuesta = await respuestaRaw.json();
    const $grafica = document.querySelector("#grafica");
    const etiquetas = respuesta.etiquetas; 
    const datosVentas2020 = {
        label: "Ventas por mes 2020",
        data: respuesta.datos, 
        backgroundColor: '#5499C7', 
        borderColor: 'RGB(202, 207, 210)', 
        borderWidth: 1,
    };
    const datosVentas2021 = {
        label: "Ventas por mes 2021",
        data: respuesta.datos2021, 
        backgroundColor: '#8E44AD', 
        borderColor: 'rgba(54, 162, 235, 1)', 
        borderWidth: 1, 
    };
    const datosVentas2022 = {
        label: "Ventas por mes 2022",
        data: respuesta.datos2022, 
        backgroundColor: 'black', 
        borderColor: 'rgba(54, 162, 235, 1)', 
        borderWidth: 1, 
    };    const datosVentas2023 = {
        label: "Ventas por mes 2021",
        data: respuesta.datos2023, 
        backgroundColor: 'blue', 
        borderColor: '#82E0AA', 
        borderWidth: 1, 
    };    const datosVentas2024 = {
        label: "Ventas por mes 2024",
        data: respuesta.datos2024, 
        backgroundColor: '#82E0AA', 
        borderColor: 'rgba(54, 162, 235, 1)', 
        borderWidth: 1, 
    };    const datosVentas2025 = {
        label: "Ventas por mes 2025",
        data: respuesta.datos2025, 
        backgroundColor: '#F4D03F', 
        borderColor: 'rgba(54, 162, 235, 1)', 
        borderWidth: 1, 
    };
    new Chart($grafica, {
        type: 'line',
        data: {
            labels: etiquetas,
            datasets: [
                datosVentas2020,
                datosVentas2021,
                datosVentas2022,
                datosVentas2023,
                datosVentas2024,
                datosVentas2025,

            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
            },
        }
    });
})();