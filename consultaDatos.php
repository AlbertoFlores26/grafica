<?php

require_once "conexion.php";

$conexion = conexion();
$sql  = " SELECT SUM(`Dato1`+`Dato2`) ,
 SUM(`Dato2`+`Dato3`) ,
 SUM(`Dato3`+`Dato4`) ,
 SUM(`Dato5`+`Dato8`) ,
 SUM(`Dato7`+`Dato4`) ,
 SUM(`Dato6`+`Dato3`) ,
 SUM(`Dato2`+`Dato4`) ,
 SUM(`Dato8`+`Dato13`) ,
 SUM(`Dato13`+`Dato14`) ,
 SUM(`Dato12`+`Dato13`) ,
 SUM(`Dato10`+`Dato10`) ,
 SUM(`Dato8`+`Dato8`) ,
 SUM(`Dato7`+`Dato7`) ,
 SUM(`Dato12`+`Dato8`) ,
 SUM(`Dato5`+`Dato4`) ,
 SUM(`Dato10`+`Dato4`) ,
 SUM(`Dato4`+`Dato5`) 
 from informacion
 GROUP BY `nombre` limit 1  ";
 
$resultados = mysqli_query($conexion, $sql);

while( $ver = mysqli_fetch_row($resultados) ){
	$datosVentas[]=$ver[0]; 
	$datosVentas[]=$ver[1]; 
	$datosVentas[]=$ver[2]; 	
	$datosVentas[]=$ver[3];
    
    
    $datosVentas2021[]=$ver[4]; 
	$datosVentas2021[]=$ver[5]; 
	$datosVentas2021[]=$ver[6]; 	
	$datosVentas2021[]=$ver[7];

    $datosVentas2022[]=$ver[8]; 
	$datosVentas2022[]=$ver[9]; 
	$datosVentas2022[]=$ver[10]; 	
	$datosVentas2022[]=$ver[11];

    $datosVentas2023[]=$ver[12]; 
	$datosVentas2023[]=$ver[13]; 
	$datosVentas2023[]=$ver[14]; 	
	$datosVentas2023[]=$ver[0];

    $datosVentas2024[]=$ver[1]; 
	$datosVentas2024[]=$ver[2]; 
	$datosVentas2024[]=$ver[3]; 	
	$datosVentas2024[]=$ver[11];
    
    $datosVentas2025[]=$ver[5]; 
	$datosVentas2025[]=$ver[2]; 
	$datosVentas2025[]=$ver[10]; 	
	$datosVentas2025[]=$ver[11];
    
}



$etiquetas = ["Agosto", "Septiembre", "Octubre", "Diciembre"];

$respuesta = [
    "etiquetas" => $etiquetas,
    "datos" => $datosVentas,
    "datos2021" => $datosVentas2021,
    "datos2022" => $datosVentas2022,
    "datos2023" => $datosVentas2023,
    "datos2024" => $datosVentas2024,
    "datos2025" => $datosVentas2025,
];

echo json_encode($respuesta);
?>