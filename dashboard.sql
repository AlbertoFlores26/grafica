-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-07-2021 a las 02:33:45
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `grafica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos`
--

CREATE TABLE `informacion` (
  `#` int(10) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `Dato1` varchar(255) NOT NULL,
  `Dato2` varchar(255) NOT NULL,
  `Dato3` varchar(255) NOT NULL,
  `Dato4` varchar(255) NOT NULL,
  `Dato5` varchar(255) NOT NULL,
  `Dato6` varchar(255) NOT NULL,
  `Dato7` varchar(255) NOT NULL,
  `Dato8` varchar(255) NOT NULL,
  `Dato9` varchar(255) NOT NULL,
  `Dato10` varchar(255) NOT NULL,
  `Dato11` varchar(255) NOT NULL,
  `Dato12` varchar(255) NOT NULL,
  `Dato13` varchar(255) NOT NULL,
  `Dato14` varchar(255) NOT NULL,
  `Dato15` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `datos`
--

INSERT INTO `informacion` (`#`, `nombre`, `Dato1`, `Dato2`, `Dato3`, `Dato4`, `Dato5`, `Dato6`, `Dato7`, `Dato8`, `Dato9`, `Dato10`, `Dato11`, `Dato12`, `Dato13`, `Dato14`, `Dato15`) VALUES
(1, 'Primero', '200', '200', '300', '300', '200', '100', '270', '150', '600', '400', '800', '100', '400', '916', '440'),
(2, 'Segundo', '150', '160', '150', '100', '100', '133', '300', '300', '500', '900', '900', '200', '501', '400', '741'),
(3, 'Tercero', '200', '250', '350', '200', '150', '250', '150', '150', '300', '400', '200', '100', '203', '400', '550');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datos`
--
ALTER TABLE `informacion`
  ADD PRIMARY KEY (`#`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `datos`
--
ALTER TABLE `informacion`
  MODIFY `#` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
